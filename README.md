API simple que se conecta a MongoDB de recetas de jugos.

Que necesitamos?
- ExpressJS
- Mongoose


Continene los siguientes endpoints:

Crear usuario:
Ruta: localhost:3000/new_user
Payload:
Ej:
  {
  	"schema": 0,
  	"name": "Felix"
  }


Crear Receta:
Ruta: localhost:3000/new_recipe
Payload:
Ej:
  {
      "schema": 0,
      "userId": "640a46b8b9447ba3c4832c3d",
      "name": "jugo de banana",
      "ingredients": [
          {
              "name": "banana",
              "qty": 2,
              "unit": "unit"
          },
          {
              "name": "leche",
              "qty": 300,
              "unit": "ml"
          }
      ],
      "method": "instrucciones"
  }


Recetas por ingredientes:
Ruta: localhost:3000/recipesbyingredient
Payload:
Ej:
  {
  	"ingredients": [
  		{
  			"name": "banana"
  		},
  		{
  			"name": "leche"
  		}
  	]
  }

Recetas por usuario:
Ruta: localhost:3000/recipes
Payload:
Ej:
  {
      "userId": "640a46b8b9447ba3c4832c3d"
  }




