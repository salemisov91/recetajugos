const express = require('express');

const mongoose = require('mongoose');

const app = express();

const userSchema = require('./models/user')

const recipeSchema = require('./models/recipe')

//Middleware
app.use(express.json())

// Routes
app.post('/new_user', (req,res) => {
    res.send(req.body)
})

app.post('/new_recipe', (req,res) => {
    res.send(req.body)
})

app.post('/rate', (req,res) => {
    res.send(req.body)
})

app.get('/recipes', (req,res) => {
    res.send("Enviamos las recetas")
})

app.listen(3000, () => console.log("Esperando en puerto 3000..."))

// MongoDB connection
mongoose.connect('mongodb://poli:password@localhost:27017/recetajugos?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))

app.post('/new_user', (req,res) => {
    const user = userSchema(req.body);
    user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.get('/recipes', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.find()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.get('/recipesbyingredient', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.find({"ingredients": {$in:[req.body]}})
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})